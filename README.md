# Tugas Slicing dengan CodeIgniter

Repositori ini dibuat untuk menyelesaikan tugas yang diberikan oleh PT Arkatama Multisolusindo dalam program SIB Fullstack Web Development.  

### Pembuat
Jason Surya Faylim, FWD1

### Informasi Tambahan
Projek dibangun menggunakan PHP 8.1  
Harap menyesuaikan konfigurasi aplikasi sebelum digunakan.

### Website
http://jasonsuryafaylim.infinityfreeapp.com/