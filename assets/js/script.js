document.addEventListener('DOMContentLoaded', function(){
  const hamburger = document.getElementById('hamburger');
  const navList = document.getElementById('navbar-navlist');

  hamburger.addEventListener('click', function(){
    if(navList.classList.contains('w-0')){
      navList.classList.remove('w-0', 'p-0');
      navList.classList.add('w-full', 'pl-5');
    } else {
      navList.classList.remove('w-full', 'pl-5');
      navList.classList.add('w-0', 'p-0');
    }
  });
});