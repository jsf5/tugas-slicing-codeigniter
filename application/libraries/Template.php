<?php

class Template{
  private $ci;

  public function __construct(){
    $this->ci = &get_instance();
  }

  public function load(string $page, $title = 'A Page', array $data = [], array $head = [], $noHeader = false, $noFooter = false){
    $_head = [
      'title' => $title,
      'styles' => [],
      'scripts' => []
    ];

    $_head['styles'] = [
      [
        'url' => 'https://fonts.googleapis.com',
        'rel' => 'preconnect'
      ],
      [
        'url' => 'https://fonts.gstatic.com',
        'rel' => 'preconnect'
      ],
      [
        'url' => 'https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap',
        'rel' => 'stylesheet'
      ],
      [
        'url' => site_url('/assets/css/all.min.css'),
        'rel' => 'stylesheet'
      ],
      [
        'url' => site_url('/assets/css/normalize.css'),
        'rel' => 'stylesheet'
      ],
      [
        'url' => site_url('/assets/css/windi.css'),
        'rel' => 'stylesheet'
      ],
      [
        'url' => site_url('/assets/css/style.css'),
        'rel' => 'stylesheet'
      ]
    ];

    $_head['scripts'] = [
      site_url("/assets/js/script.js"),
    ];

    if(array_key_exists('styles', $head)){
      $_head = array_merge($_head['styles'], $head['styles']);
    }

    if(array_key_exists('scripts', $head)){
      $_head = array_merge($_head['scripts'], $head['scripts']);
    }

    $this->ci->load->view("template", [
      'page' => "pages/$page",
      'head' => $_head,
      'data' => $data,
      'noHeader' => $noHeader,
      'noFooter' => $noFooter
    ]);
  }
}

?>