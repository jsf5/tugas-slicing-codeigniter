<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title><?= $title ?></title>

<?php foreach($styles as $style): ?>
<link href="<?= $style['url'] ?>" rel="<?= $style['rel'] ?>" />
<?php endforeach; ?>

<?php foreach($scripts as $script): ?>
<script src="<?= $script ?>" defer></script>
<?php endforeach; ?>