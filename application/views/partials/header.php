<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<header class="sticky top-0 z-20">
  <nav class="bg-light flex py-5 items-center container h-20">
    <a href="<?= site_url('/') ?>" class="text-primary text-xl font-700"> WeBuildWebs </a>
    <button class="block md:hidden bg-transparent border-none text-3xl ml-auto text-primary" id="hamburger">
      <i class="fa-solid fa-bars"></i>
    </button>
    <ul class="md:static md:flex-row md:gap-10 md:w-auto md:min-h-max md:max-w-max right-0 top-20 overflow-hidden flex flex-col fixed ml-auto gap-5 p-0 list-none font-500 m-0 transition-all overflow-hidden max-w-72 w-0 bg-light min-h-100vh z-100" id="navbar-navlist">
      <li>
        <a href="<?= site_url('#services') ?>" class="text-secondary"> Services </a>
      </li>
      <li>
        <a href="<?= site_url('#portfolio') ?>" class="text-secondary"> Portfolio </a>
      </li>
      <li>
        <a href="<?= site_url('#reviews') ?>" class="text-secondary"> Reviews </a>
      </li>
      <li>
        <a href="<?= site_url('login') ?>" class="text-black p-0 bg-transparent border-none">
          Login
        </a>
      </li>
    </ul>
  </nav>
</header>