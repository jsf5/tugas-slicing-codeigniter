<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<footer class="relative mt-40 pt-24 pb-10 text-light bg-secondary">
  <div class="contact-card bg-primary w-9/10 md:w-auto">
    <div class="text-white text-2xl font-bold">Interested? Get in touch with us.</div>
    <button class="btn float-right mt-4">
      Contact Us
    </button>
  </div>
  <div class="container grid gap-10 items-center grid-cols-1 md:grid-cols-2 xl:grid-cols-3">
    <div class="pr-5">
      <div class="font-bold text-xl">
        Subscribe to our newsletter
      </div>
      <div class="form-control mb-3 bg-light text-primary">
        <label for="email">
          <i class="fa-regular fa-envelope"></i>
        </label>
        <input type="email" id="email" placeholder="Email" />
      </div>
      <button class="btn">
        Subscribe
      </button>
    </div>
    <div class="text-light text-4xl font-700 text-center">
      WeBuildWebs
    </div>
    <ul class="flex flex-col gap-8 font-semibold list-none items-end">
      <li><a href="#">Privacy Policy</a></li>
      <li><a href="#">Terms & Conditions</a></li>
      <li><a href="#">FAQ</a></li>
    </ul>
  </div>
</footer>