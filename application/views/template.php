<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('partials/head', $head); ?>
</head>
<body>
  <?php
    if(!$noHeader){
      $this->load->view('partials/header');
    }
  ?>
  <main>
    <?php $this->load->view("$page"); ?>
  </main>
  <?php
    if(!$noFooter){
      $this->load->view('partials/footer');
    }
  ?>
</body>
</html>