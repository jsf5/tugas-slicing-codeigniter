<main>
  <section class="flex gap-10 bg-light container pt-5">
    <div class="w-full md:w-3/5 xl:w-7/10 text-primary">
      <h1 class="xl:text-2xl 2xl:text-4xl">Need a custom build website?</h1>
      <span class="xl:text-5xl 2xl:text-7xl font-bold">Let us build it for you</span>
      <p class="xl:text-lg 2xl:text-2xl">We are experienced in building websites for  e-commerse, business profiles, blogs, and many more. We also take requests for complex business requirements.</p>
    </div>
    <div class="hidden md:block w-2/5 xl:w-3/10">
      <img src="./assets/img/Website-PNG-Images-HD.png" alt="Placeholder 1" class="w-full object-contain" />
    </div>
  </section>
  <div class="wave" style="--translate-y: -50px; height: 216px; --wave: url('../img/first-wave.svg')"></div>
  <section class="container">
    <div class="text-center text-4xl md:text-5xl text-primary font-bold">
      Trusted By
    </div>
    <div class="flex flex-wrap gap-20 justify-center my-10">
      <img src="./assets/img/logo-1.svg" alt="Logo 1" />
      <img src="./assets/img/logo-2.svg" alt="Logo 2" />
      <img src="./assets/img/logo-3.svg" alt="Logo 3" />
      <img src="./assets/img/logo-4.svg" alt="Logo 4" />
    </div>
  </section>
  <div class="wave" style="--translate-y: 50px; height: 200px; --wave: url('../img/second-wave.svg')"></div>
  <section class="bg-primary container text-light relative z-4" id="services">
    <h1 class="text-5xl">Our Services</h1>
    <div class="flex gap-10 flex-wrap justify-center">
      <div class="rounded border-light border-2 border-solid w-75 px-5 py-3 flex flex-col items-center">
        <div class="text-9xl">
          <i class="fa-brands fa-wordpress"></i>
        </div>
        <p class="text-xl font-bold text-center">
          We are experienced in using Wordpress to buld websites. If you only need a simple website for ecommerce or blogs, this is the ideal solution for you
        </p>
      </div>
      <div class="rounded border-light border-2 border-solid w-75 px-5 py-3 flex flex-col items-center">
        <div class="text-9xl">
          <svg width="120" height="120" viewBox="0 0 120 120" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M70 35C68.6739 35 67.4021 35.5268 66.4645 36.4645C65.5268 37.4021 65 38.6739 65 40V80C65 81.3261 65.5268 82.5979 66.4645 83.5355C67.4021 84.4732 68.6739 85 70 85H90C91.3261 85 92.5979 84.4732 93.5355 83.5355C94.4732 82.5979 95 81.3261 95 80V40C95 38.6739 94.4732 37.4021 93.5355 36.4645C92.5979 35.5268 91.3261 35 90 35H70ZM85 45H75V75H85V45Z" fill="#FBEEFF"/>
            <path d="M30 35C28.6739 35 27.4021 35.5268 26.4645 36.4645C25.5268 37.4021 25 38.6739 25 40C25 41.3261 25.5268 42.5979 26.4645 43.5355C27.4021 44.4732 28.6739 45 30 45H50C51.3261 45 52.5979 44.4732 53.5355 43.5355C54.4732 42.5979 55 41.3261 55 40C55 38.6739 54.4732 37.4021 53.5355 36.4645C52.5979 35.5268 51.3261 35 50 35H30ZM30 55C28.6739 55 27.4021 55.5268 26.4645 56.4645C25.5268 57.4021 25 58.6739 25 60C25 61.3261 25.5268 62.5979 26.4645 63.5355C27.4021 64.4732 28.6739 65 30 65H50C51.3261 65 52.5979 64.4732 53.5355 63.5355C54.4732 62.5979 55 61.3261 55 60C55 58.6739 54.4732 57.4021 53.5355 56.4645C52.5979 55.5268 51.3261 55 50 55H30ZM25 80C25 78.6739 25.5268 77.4021 26.4645 76.4645C27.4021 75.5268 28.6739 75 30 75H50C51.3261 75 52.5979 75.5268 53.5355 76.4645C54.4732 77.4021 55 78.6739 55 80C55 81.3261 54.4732 82.5979 53.5355 83.5355C52.5979 84.4732 51.3261 85 50 85H30C28.6739 85 27.4021 84.4732 26.4645 83.5355C25.5268 82.5979 25 81.3261 25 80Z" fill="#FBEEFF"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M20 15C16.0218 15 12.2064 16.5804 9.3934 19.3934C6.58035 22.2064 5 26.0218 5 30V90C5 93.9782 6.58035 97.7936 9.3934 100.607C12.2064 103.42 16.0218 105 20 105H100C103.978 105 107.794 103.42 110.607 100.607C113.42 97.7936 115 93.9782 115 90V30C115 26.0218 113.42 22.2064 110.607 19.3934C107.794 16.5804 103.978 15 100 15H20ZM100 25H20C18.6739 25 17.4021 25.5268 16.4645 26.4645C15.5268 27.4021 15 28.6739 15 30V90C15 91.3261 15.5268 92.5979 16.4645 93.5355C17.4021 94.4732 18.6739 95 20 95H100C101.326 95 102.598 94.4732 103.536 93.5355C104.473 92.5979 105 91.3261 105 90V30C105 28.6739 104.473 27.4021 103.536 26.4645C102.598 25.5268 101.326 25 100 25Z" fill="#FBEEFF"/>
          </svg>                
        </div>
        <p class="text-xl font-bold text-center">
          We are capable of bulding custom websites for complex business cases. Please contact us to discuss your website requirements 
        </p>
      </div>
      <div class="rounded border-light border-2 border-solid w-75 px-5 py-3 flex flex-col items-center">
        <div class="text-9xl">
          <i class="fa-solid fa-mobile-screen"></i>
        </div>
        <p class="text-xl font-bold text-center">
          We are also capable of building mobile applications for iOS or Android. Please contact us to discuss your website reqirements.
        </p>
      </div>
    </div>
  </section>
  <div class="wave" style="--translate-y: -65px; height: 255px; --wave: url('../img/third-wave.svg'); z-index: 1"></div>
  <section class="bg-red-200 container pb-20" style="margin-top: -255px; padding-top: 200px;" id="portfolio">
    <h1 class="text-5xl text-primary">Our Portfolio</h1>
    <div class="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-10 justify-items-center">
      <div class="portfolio-card">
        <div class="portfolio-image">
          <img src="./assets/img/p-1.png" alt="Portofolio" />
          <a href="#" class="bg-primary bg-opacity-90 rounded-tl-full">Check it out</a>
        </div>
      </div>
      <div class="portfolio-card">
        <div class="portfolio-image">
          <img src="./assets/img/p-2.png" alt="Portofolio" />
          <a href="#" class="bg-primary bg-opacity-90 rounded-tl-full">Check it out</a>
        </div>
      </div>
      <div class="portfolio-card">
        <div class="portfolio-image">
          <img src="./assets/img/p-3.png" alt="Portofolio" />
          <a href="#" class="bg-primary bg-opacity-90 rounded-tl-full">Check it out</a>
        </div>
      </div>
      <div class="portfolio-card">
        <div class="portfolio-image">
          <img src="./assets/img/p-4.png" alt="Portofolio" />
          <a href="#" class="bg-primary bg-opacity-90 rounded-tl-full">Check it out</a>
        </div>
      </div>
      <div class="portfolio-card">
        <div class="portfolio-image">
          <img src="./assets/img/p-5.png" alt="Portofolio" />
          <a href="#" class="bg-primary bg-opacity-90 rounded-tl-full">Check it out</a>
        </div>
      </div>
      <div class="portfolio-card">
        <div class="portfolio-image">
          <img src="./assets/img/p-6.png" alt="Portofolio" />
          <a href="#" class="bg-primary bg-opacity-90 rounded-tl-full">Check it out</a>
        </div>
      </div>
    </div>
  </section>
  <section id="reviews" class="my-20">
    <h1 class="container text-primary mb-10">
      What others say about us
    </h1>
    <div class="grid grid-cols-1 xl:grid-cols-2 px-10 xl:px-30 gap-10">
      <div class="flex flex-col md:flex-row items-center gap-6 items-center">
        <div class="profile-picture">
          <img src="./assets/img/placeholder-person.png" />
        </div>
        <p class="leading-6 text-xl font-semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel vitae veritatis dolor ullam molestiae cumque, ducimus ab odio rerum hic mollitia repellendus suscipit architecto accusantium quos!</p>
      </div>
      <div class="flex flex-col md:flex-row items-center gap-6 items-center">
        <div class="profile-picture">
          <img src="./assets/img/placeholder-person.png" />
        </div>
        <p class="leading-6 text-xl font-semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel vitae veritatis dolor ullam molestiae cumque, ducimus ab odio rerum hic mollitia repellendus suscipit architecto accusantium quos!</p>
      </div>
      <div class="flex flex-col md:flex-row items-center gap-6 items-center">
        <div class="profile-picture">
          <img src="./assets/img/placeholder-person.png" />
        </div>
        <p class="leading-6 text-xl font-semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel vitae veritatis dolor ullam molestiae cumque, ducimus ab odio rerum hic mollitia repellendus suscipit architecto accusantium quos!</p>
      </div>
      <div class="flex flex-col md:flex-row items-center gap-6 items-center">
        <div class="profile-picture">
          <img src="./assets/img/placeholder-person.png" />
        </div>
        <p class="leading-6 text-xl font-semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel vitae veritatis dolor ullam molestiae cumque, ducimus ab odio rerum hic mollitia repellendus suscipit architecto accusantium quos!</p>
      </div>
      <div class="flex flex-col md:flex-row items-center gap-6 items-center">
        <div class="profile-picture">
          <img src="./assets/img/placeholder-person.png" />
        </div>
        <p class="leading-6 text-xl font-semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel vitae veritatis dolor ullam molestiae cumque, ducimus ab odio rerum hic mollitia repellendus suscipit architecto accusantium quos!</p>
      </div>
      <div class="flex flex-col md:flex-row items-center gap-6 items-center">
        <div class="profile-picture">
          <img src="./assets/img/placeholder-person.png" />
        </div>
        <p class="leading-6 text-xl font-semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel vitae veritatis dolor ullam molestiae cumque, ducimus ab odio rerum hic mollitia repellendus suscipit architecto accusantium quos!</p>
      </div>
    </div>
  </section>
</main>