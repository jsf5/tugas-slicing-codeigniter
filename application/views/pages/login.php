<div class="flex mt-20">
  <div class="m-auto bg-secondary px-10 rounded pb-20 mb-10">
    <h1 class="text-light text-4xl font-700 text-center my-15">WeBuildWebs</h1>
    <form class="my-10 flex flex-col gap-10">
      <div class="form-control mb-3 bg-light text-secondary">
        <input type="email" id="email" placeholder="Email" />
        <label for="email">
          <i class="fa-regular fa-user"></i>
        </label>
      </div>
      <div class="form-control mb-3 bg-light text-secondary">
        <input type="password" id="email" placeholder="Password" />
        <label for="email">
          <i class="fa-solid fa-key"></i>
        </label>
      </div>
    </form>
    <div class="text-center"><button class="btn px-10">Login</button></div>
    <div class="text-2xl text-white font-semibold mt-10 text-center">
      <span class="block">Don't have an account?</span>
      <a href="#" class="underline">Register here</a>
    </div>
  </div>
</div>